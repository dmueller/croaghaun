#cython: boundscheck=False, wraparound=False, nonecheck=False, cdivision=True
import time

from cython.parallel cimport prange, parallel
from cython.parallel import prange, parallel
import numpy as np
cimport numpy as cnp
from libc.stdlib cimport malloc, free
from libc.math cimport fabs

"""
    Simulation class (holds all fields)
"""
class Simulation:
    def __init__(self, nt, nr, dt, dr):
        # lattice size and spacings
        self.nt = nt
        self.nr = nr
        self.dt = dt
        self.dr = dr

        # A_\rho, A_\theta, A_y
        self.ar = np.zeros((nt, nr), dtype=np.double)
        self.at = np.zeros((nt, nr), dtype=np.double)
        self.ay = np.zeros((nt, nr), dtype=np.double)

        # beta (1 + alpha)
        self.b = np.ones((nt, nr), dtype=np.double)

        # field strengths
        self.el = np.zeros((nt, nr), dtype=np.double)
        self.bl = np.zeros((nt, nr), dtype=np.double)
        self.et = np.zeros((nt, nr), dtype=np.double)
        self.bt = np.zeros((nt, nr), dtype=np.double)

        # lagrange density
        self.l = np.zeros((nt, nr), dtype=np.double)

    def solve(self):
        t0 = time.time()
        solve(self)
        field_strengths(self)
        lagrange(self)
        print("Complete after {} s.".format(round(time.time() - t0, 5)))

    """
        Initialize longitudinal electric field
    """
    def init_el(self, el0):
        self.ay[0, :] = 0.0
        self.ay[1, :] = 0.5 * self.dt ** 2 * el0[:]

        # boundary condition
        self.ay[1, 0] = self.ay[1, 1]
        self.ay[1, -1] = 0.0

    """
        Initialize longitudinal magnetic field
    """
    def init_bl(self, bl0):
        bl0_avg = 0.5 * (bl0[1:] + bl0[:-1])
        self.at[0, 0] = 0.0
        for r in range(1, self.nr):
            self.at[0, r] = - self.dr ** 2 * (r - 0.5) * bl0_avg[r-1] + self.at[0, r-1]

        # boundary condition
        self.at[0, 0] = 0.0
        self.at[0, -1] = self.at[0, -2]

        self.at[1, :] = self.at[0, :]


"""
    Solve time evolution for all fields up to maximum time
"""
def solve(s):
    cdef cnp.ndarray[double, ndim=2, mode="c"] ar = s.ar
    cdef cnp.ndarray[double, ndim=2, mode="c"] at = s.at
    cdef cnp.ndarray[double, ndim=2, mode="c"] ay = s.ay
    cdef cnp.ndarray[double, ndim=2, mode="c"] b = s.b

    cdef double dt = s.dt
    cdef double dr = s.dr

    cdef int nt = s.nt
    cdef int nr = s.nr
    cdef double w1, w2, w3, w4
    cdef int r, t

    for t in range(1, nt-1):
        # interior cells
        for r in prange(1, nr-1, nogil=True):
            # EOM for A_\rho component
            ar[t+1, r] = ar[t, r] + ((b[t  , r] + b[t-1, r]) * (t - 0.5)) / ((b[t+1, r] + b[t  , r]) * (t + 0.5)) * \
                         (ar[t-1, r] + ar[t, r])

            # EOM for A_\theta component
            w1 = 0.5 * (b[t+1,r] + b[t,  r]) * (t + 0.5) * dt / (r * dr)
            w2 = 0.5 * (b[t,r  ] + b[t-1,r]) * (t - 0.5) * dt / (r * dr)
            w3 = 0.5 * (b[t,r  ] + b[t,r-1]) * t * dt / ((r - 0.5) * dr)
            w4 = 0.5 * (b[t,r+1] + b[t,r  ]) * t * dt / ((r + 0.5) * dr)

            at[t+1, r] = - dr ** 2 * at[t-1, r] * w2 + dt ** 2 * (at[t, r-1] * w3 + at[t, r+1] * w4) + \
                         at[t, r] * (- dt ** 2 * (w3 + w4) + dr ** 2 * (w1 + w2))
            at[t+1, r] /= dr ** 2 * w1

            # EOM for A_y component
            w1 = 0.5 * (b[t+1,r] + b[t,r  ]) * r * dr / ((t + 0.5) * dt)
            w2 = 0.5 * (b[t-1,r] + b[t,r  ]) * r * dr / ((t - 0.5) * dt)
            w3 = 0.5 * (b[t,r  ] + b[t,r-1]) * (r - 0.5) * dr / (t * dt)
            w4 = 0.5 * (b[t,r  ] + b[t,r+1]) * (r + 0.5) * dr / (t * dt)

            ay[t+1, r] = - dr ** 2 * ay[t-1, r] * w2 + dt ** 2 * (ay[t, r-1] * w3 + ay[t, r+1] * w4) + \
                         ay[t, r] * (- dt ** 2 * (w3  + w4) + dr ** 2 * (w1 + w2))
            ay[t+1, r] /= dr ** 2 * w1

        # boundary cells
        ar[t+1, 0] = ar[t+1, 1]
        at[t+1, 0] = 0.0
        ay[t+1, 0] = ay[t+1, 1]

        ar[t+1, nr-1] = ar[t+1, nr-2]
        at[t+1, nr-1] = at[t+1, nr-2]
        ay[t+1, nr-1] = 0.0


"""
    Compute field strengths: EL, BL, ET, BT
"""
def field_strengths(s):
    cdef cnp.ndarray[double, ndim=2, mode="c"] ar = s.ar
    cdef cnp.ndarray[double, ndim=2, mode="c"] at = s.at
    cdef cnp.ndarray[double, ndim=2, mode="c"] ay = s.ay
    cdef cnp.ndarray[double, ndim=2, mode="c"] b = s.b

    cdef cnp.ndarray[double, ndim=2, mode="c"] el = s.el
    cdef cnp.ndarray[double, ndim=2, mode="c"] bl = s.bl
    cdef cnp.ndarray[double, ndim=2, mode="c"] et = s.et
    cdef cnp.ndarray[double, ndim=2, mode="c"] bt = s.bt

    cdef double dt = s.dt
    cdef double dr = s.dr

    cdef int nt = s.nt
    cdef int nr = s.nr
    cdef int r, t

    el[:, :] = 0
    bl[:, :] = 0
    et[:, :] = 0
    bt[:, :] = 0

    for t in range(0, nt-1):
        for r in prange(0, nr, nogil=True):
            el[t, r] = + (ay[t+1, r] - ay[t, r]) / (dt ** 2 * (t + 0.5))

    for t in range(0, nt):
        for r in prange(0, nr-1, nogil=True):
            bl[t, r] = - (at[t, r+1] - at[t, r]) / (dr ** 2 * (r + 0.5))

    for t in range(0, nt-1):
        for r in prange(1, nr, nogil=True):
            et[t, r] = + (at[t+1, r] - at[t, r]) / (dr * dt * r)

    for t in range(1, nt):
        for r in prange(0, nr-1, nogil=True):
            bt[t, r] = - (ay[t, r+1] - ay[t, r]) / (dr * dt * t)

    # re-average
    el[:, 0:-1] = 0.5 * (el[:, 0:-1] + el[:, 1:])
    bl[0:-1, :] = 0.5 * (bl[0:-1, :] + bl[1:, :])
    et[:, 0:-1] = 0.5 * (et[:, 0:-1] + et[:, 1:])
    bt[0:-1, :] = 0.5 * (bt[0:-1, :] + bt[1:, :])

def lagrange(s):
    cdef cnp.ndarray[double, ndim=2, mode="c"] el = s.el
    cdef cnp.ndarray[double, ndim=2, mode="c"] bl = s.bl
    cdef cnp.ndarray[double, ndim=2, mode="c"] et = s.et
    cdef cnp.ndarray[double, ndim=2, mode="c"] bt = s.bt

    cdef cnp.ndarray[double, ndim=2, mode="c"] l = s.l

    cdef double dt = s.dt
    cdef double dr = s.dr

    cdef int nt = s.nt
    cdef int nr = s.nr
    cdef int r, t
    cdef double v

    l[:, :] = 0.0
    for t in range(0, nt-1):
        for r in prange(0, nr-1, nogil=True):
            v = (r + 0.5) * dr * (t + 0.5) * dt
            l[t, r] += el[t, r] ** 2 / 2.0 * v
            l[t, r] -= bl[t, r] ** 2 / 2.0 * v
            l[t, r] += et[t, r] ** 2 / 2.0 * v
            l[t, r] -= bt[t, r] ** 2 / 2.0 * v