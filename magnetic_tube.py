import croaghaun as c
import numpy as np
import matplotlib.pyplot as plt

# lattice size and spacing
nr = 512 * 4
nt = nr * 4
Lr = 2.0
Lt = 0.5
dr = Lr / nr
dt = Lt / nt

# create simulation object
s = c.Simulation(nt=nt, nr=nr, dt=dt, dr=dr)

# ranges of rho and tau
rho = np.linspace(0, nr * dr, num=nr)
tau = np.linspace(0, nt * dt, num=nt)

# For testing only: non-zero alpha coupling to holographic sector
#amplitude = 0.0
#for t in range(nt):
#    for r in range(nr):
#        s.b[t, r] = 1.0 - amplitude * np.exp(- (r * dr) ** 2 / 128.0) * (np.tanh(0.7 * t * dt) + 0.5)

# Initial condition: Gaussian magnetic flux tube
sig = 0.1
B0 = 1.0
BL0 = B0 * np.exp(- rho ** 2 / (2 * sig ** 2)) / np.sqrt(2 * np.pi * sig ** 2)
s.init_bl(BL0)

# Solve time evolution
s.solve()

# Plot Lagrange density
tv, rv = np.meshgrid(rho, tau)
plt.clf()
plt.title("Lagrange density")
plt.xlabel("rho")
plt.ylabel("tau")
plt.contourf(tv, rv, s.l)
plt.colorbar()
plt.contour(tv, rv, s.l, colors='black')
plt.xlim(0, Lt)
plt.ylim(0, Lr/2)
plt.show()

# Plot results (field strengths)
plt.ion()
for t in range(0, nt, int(nt / 128)):
    plt.clf()
    plt.title("tau = {}".format(t * dt))
    plt.xlabel("rho")
    plt.plot(rho, s.el[t], label="E_L")
    plt.plot(rho, s.bl[t], label="B_L")
    plt.plot(rho, s.et[t], label="E_T")
    plt.plot(rho, s.bt[t], label="B_T")
    plt.legend()
    plt.pause(0.01)

# Plot results (gauge fields)
plt.ion()
for t in range(0, nt, int(nt / 128)):
    plt.clf()
    plt.title("tau = {}".format(t * dt))
    plt.xlabel("rho")
    plt.ylabel("A_mu")
    plt.plot(rho, s.ar[t], label="A_rho")
    plt.plot(rho, s.at[t], label="A_theta")
    plt.plot(rho, s.ay[t], label="A_y")
    plt.legend()
    plt.pause(0.01)
